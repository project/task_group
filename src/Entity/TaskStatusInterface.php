<?php

namespace Drupal\task_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Task Status entities.
 */
interface TaskStatusInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
