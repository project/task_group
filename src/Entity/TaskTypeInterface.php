<?php

namespace Drupal\task_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Task type entities.
 */
interface TaskTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
