<?php

namespace Drupal\task_api\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Task entities.
 *
 * @ingroup task_api
 */
class TaskDeleteForm extends ContentEntityDeleteForm {


}
